#!/bin/bash

# shellcheck disable=SC1091

set -o errexit
set -o nounset
set -o pipefail
# set -o xtrace # Uncomment this line for debugging purpose

# Load libraries
. /opt/bitnami/scripts/libpostgresql.sh
. /opt/bitnami/scripts/libos.sh

# Load PostgreSQL environment variables
eval "$(postgresql_env)"

if [ $BACKUP_ON_INIT = true ]; then

cat <<EOF
******************
* Backup mode on first database initialisation detected
*
* This script will only run once
*
* postgresql.conf and pg_hba.conf files will be included in the full backup
* 
* Starting full backup
******************
EOF
    
    export PGPASSWORD="${POSTGRESQL_PASSWORD:-$POSTGRES_PASSWORD}"
    export PGUSER="postgres"

    # Backup postgresql.conf and pg_hba too
    cp $POSTGRESQL_CONF_FILE $POSTGRESQL_DATA_DIR/
    cp $POSTGRESQL_PGHBA_FILE $POSTGRESQL_DATA_DIR/

    wal-g backup-push "$POSTGRESQL_DATA_DIR"

    # Restore PGDATA dir state
    rm $POSTGRESQL_DATA_DIR/postgresql.conf
    rm $POSTGRESQL_DATA_DIR/pg_hba.conf

else
cat <<EOF
******************
* Backup mode on first database initialisation disabled
*
* Don't forget to use wal-g backup-push in order to have a recoverable instance
******************
EOF
fi
