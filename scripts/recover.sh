#!/bin/bash

# shellcheck disable=SC1091

set -o errexit
set -o nounset
set -o pipefail
# set -o xtrace # Uncomment this line for debugging purpose

# Load libraries
. /opt/bitnami/scripts/libpostgresql.sh
. /opt/bitnami/scripts/libos.sh

# Load PostgreSQL environment variables
eval "$(postgresql_env)"

if [ -d $POSTGRESQL_DATA_DIR/pg_wal ]; then

cat <<EOF
******************
* It appears that $POSTGRESQL_DATA_DIR is not empty
* Skipping recovery
******************
EOF

else

cat <<EOF
******************
* Starting recovery
******************
EOF

    # fetch most recent full backup
    # backup should include postgresql.conf and pg_hba.conf
    echo "Fetching backup"
    wal-g backup-fetch $POSTGRESQL_DATA_DIR LATEST

    # Create mock conf.d dir in case postgresql.conf has it
    # This is the default in Bitnami's setup
    mkdir -p $POSTGRESQL_DATA_DIR/conf.d

    # enable recovery mode
    touch $POSTGRESQL_DATA_DIR/recovery.signal;

    # configure recovery mode
    echo "Configuring recovery mode"
    cat /scripts/recovery.conf >> $POSTGRESQL_DATA_DIR/postgresql.conf;

    echo "Starting Postgresql in recovery mode"
    bash -c "/opt/bitnami/postgresql/bin/pg_ctl start -D $POSTGRESQL_DATA_DIR" &

    sleep 10

    # Wait for postgresql and post-recovery script to finish
    PID=$(ps -ef | grep pg_ctl | awk {'print $2'} | head -n 1)
    tail --pid=$PID -f /dev/null

    echo "Restoring $POSTGRESQL_DATA_DIR to container original state"
    rm $POSTGRESQL_DATA_DIR/postgresql.conf
    rm $POSTGRESQL_DATA_DIR/pg_hba.conf
    rm -r $POSTGRESQL_DATA_DIR/conf.d

fi

