#!/bin/bash

# shellcheck disable=SC1091

set -o errexit
set -o nounset
set -o pipefail
# set -o xtrace # Uncomment this line for debugging purpose

# Load libraries
. /opt/bitnami/scripts/libpostgresql.sh
. /opt/bitnami/scripts/libos.sh

# Load PostgreSQL environment variables
eval "$(postgresql_env)"

POSTGRESQL_ARCHIVE_CONF_FILE=$POSTGRESQL_MOUNTED_CONF_DIR/conf.d

if [ $ARCHIVE_MODE = true ]; then

    if [ -e $POSTGRESQL_ARCHIVE_CONF_FILE ]; then

cat <<EOF
******************
* Archive configuration already in place.
* Skipping setup
******************
EOF

    else

cat <<EOF
******************
* Archive mode detected
* Adding configuration to the conf.d directory with default settings
******************
EOF
      mkdir -p $POSTGRESQL_MOUNTED_CONF_DIR/conf.d
      cp /scripts/archive.conf $POSTGRESQL_ARCHIVE_CONF_FILE
    
    fi

else

cat <<EOF
******************
* Archive mode disabled
* Not running with default archive mode settings
* If you'd like to have WAL continous backups, please mount your own postgresql configuration
******************
EOF

fi
