#!/bin/bash

# shellcheck disable=SC1091

set -o errexit
set -o nounset
set -o pipefail
# set -o xtrace # Uncomment this line for debugging purpose

cat <<EOF
******************
* Recovery mode complete; postgresql is shutting down.
******************
EOF

/opt/bitnami/postgresql/bin/pg_ctl stop -D $PGDATA


