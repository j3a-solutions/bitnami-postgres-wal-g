# Bitnami Postgres WAL-G

This image is largely inspired by [this wal-g extension of the stock postgres image](https://gitlab.koehn.com/docker/postgres-wal-g). However, instead of using the stock [Postgres Image](https://hub.docker.com/_/postgres/), it uses [Bitnami's Postgres Image](https://hub.docker.com/r/bitnami/postgresql).

In addition to gaining from all the advatanages of using a Bitnami image, this image also:

- Implements continous WAL backup using [WAL-G](https://github.com/wal-g/wal-g)
- Includes a series of scripts that are run securely by the non-root runtime user
- Allows to configure behaviour by hooking into the container lifecycle. This is particularly useful in a Kubernetes setup.
- Can perform backup and recovery of postgres data

## Environment

With no extra environment variables, this image will just run in the same way as Bitnami's image.
Every feature is opt-in by default.

### Configuring wal-g

Any environment that you pass to the container, will be picked up by wal-g.
Please refer to their [documentation](https://github.com/wal-g/wal-g) in order to setup
your S3 or S3 compatible storage for the backups.

### Configuring archive mode

There are two ways in which you can configure your container to run in archive mode:

- Use the usual mount volume paths of the Bitnami image
- Set `ARCHIVE_MODE=true` to apply a set of defaults to your configuration. The settings are:

```
archive_mode    = on
archive_command = 'wal-g wal-push %p'
archive_timeout = 60
```

## Full backups

Postgres physical replication requires both the physical files and a backup of WAL.
Wal-g manages these two as separate processes and, in order to fully restore your data, you'll need both.
By setting `BACKUP_ON_INIT=true` a full database backup will be performed on the first run of your container.
This script will only run once and it ensures that the recovery process has a base to build on.

Every time you change the configuration of your container (e.g.: `postgresql.conf`, `pg_hba.conf`, etc.) you'll need to perform the full backup again.

## Recovery

In order to recover the data, you only need to run the container in an empty volume with the command `/recover`.

For example:

```
docker run --rm \
 -v <your_volume>:/bitnami/postgresql \
 -e POSTGRESQL_PASSWORD=<password> \
 -e AWS_ACCESS_KEY_ID=<key> \
 -e AWS_SECRET_ACCESS_KEY=<secret> \
 -e WALG_S3_PREFIX=s3://<bucket> \
 -e AWS_ENDPOINT=https://<minio_endpoint> \
 -e AWS_S3_FORCE_PATH_STYLE=true \
 j3asolutions/bitnami-postgres-wal-g:12.2.0-debian-10-r67-0 /scripts/recover.sh
```

This command will:

- Start a new container with user 1001
- Recover your data using the last full backup
- Start postgres in recovery mode and restore WAL
- Destroy the container afterwards

After the container has completed, you can start your other container as you wish by mounting the data volume in it. You may even use a different image in your other container if you wish.

## A note on priviliges and non-root containers

### Running scripts

Bitnami images add an extra layer of security by not running containers with `root` user. All wal-g scripts will be located in the `/scripts` folder, but in order to execute them you'll need to bear in mind that:

- if you're running this image in a docker container, you should execute commands using the root user: `docker exec -u 0 -it my-container /scripts/backup`
- if you're running this image in Kubernetes, the Bitnami helm chart will set up a user for your, and you may not need to execute commands using `root` user

### Postgres superuser and backups

Wal-g needs to run with a `PGUSER` which has superuser privileges. This user has to be able to, for example, call `pg_stop_backup`. The default `PGUSER` is `postgres` but if you set an extra user and database, the `postgres` user won't have a password and wouldn't be able to login using md5. In this case you'll need to add your own `pg_hba.conf` to trust connections by the user `postgres` in localhost. In this case you should set `BACKUP_ON_INIT=false` and run full backups using docker or kubectl `exec`.
