ARG BITNAMI_POSTGRES_IMAGE

FROM bitnami/postgresql:$BITNAMI_POSTGRES_IMAGE

ENV POSTGRESQL_ARCHIVE_CONF_FILE=$PG_CONFD_PATH/archive.conf
ENV BACKUP_ON_INIT=false
ENV ARCHIVE_MODE=false

COPY wal-g /usr/local/bin/wal-g

COPY scripts /scripts

COPY scripts/10_archive.sh /docker-entrypoint-preinitdb.d/

COPY scripts/20_backup.sh  /docker-entrypoint-initdb.d/